/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import client.mqtt.Publisher;
import javax.swing.JTextArea;

import client.mqtt.Subscriber;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author Mallory
 */
public class ReadWriteCommand {
    ReadCommand readC; 
    WriteCommand writedC;
    final String topic;
    final int finalTimeSendC = 0;//times of tryies
    ReadWriteCommand(JTextArea logOutput) throws InterruptedException{
        this.readC = new ReadCommand(logOutput);  
        this.writedC = new WriteCommand(readC.comPort);
        this.readC.writedC = this.writedC;
             
        //enabling receive SMS
        sendCommandsim900("at",2000,"OK",false); 
//        sendCommandsim900("AT+CSCS=\"GSM\"",2000,"OK",false); 
//        sendCommandsim900("AT+CMGF=1",2000,"OK",false);   
//        sendCommandsim900("AT+CREG=1",2000,"OK",false); 
//        sendCommandsim900("AT+CMMS=2",2000,"OK"); //helps send sms faster
//        sendCommandsim900("AT+GSMBUSY=2",2000,"OK"); //helps send sms faster
        sendCommandsim900("AT+CNMI=2,2,0,0,0",2000,"OK",false);//AT+CNMI=1,2,0,0,0 //at+cnmi=2,1 or 2,0 e limpar quando chegar new message
       
        this.topic = "/dev/newSMS";  
        try {
            Subscriber.main(topic, this);
        } catch (MqttException ex) {
            Logger.getLogger(ReadWriteCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    boolean sendCommandsim900(String command,int timeWait,String response, boolean setReadyRead) throws InterruptedException{
        boolean result = true;
        int timeTriedSend = 0; 
        String lineOutput;
        do{
            readC.cleanLineOutput();
            writedC.writeCommandAT(command,timeWait);
            while(writedC.t1.isAlive());
            if(setReadyRead)//check if it ready to read for send sms
                readC.setreadyRead(false);
            while(!readC.getreadyRead()){//wait until receive data from hardare
                Thread.sleep(100); 
//                System.out.println("valor"+readC.getreadyRead());
            }
//            System.out.println(readC.getLineOutput());
            timeTriedSend ++;
            lineOutput = readC.getLineOutput();
        }while((!lineOutput.contains(response)) && (timeTriedSend < this.finalTimeSendC));
        
        //to check if it ok, need to wait when receive the correct response from readCommand
//        if(!lineOutput.contains(response)&& (!lineOutput.contains(">") && (!lineOutput.contains("+CMGS")))){//old code timeTriedSend==this.finalTimeSendC
//            //send Error to server too
//            System.out.println("Error sending the correct Command: "+command+", Expected Response: "+response+", Command+Response:"+readC.getLineOutput()+", Time Wait: "+timeWait);
//            result = false;
//        }
        readC.cleanLineOutput();
        return result;
    } 
    
    public void sendSMSsim900(String inputCellNumber, String message, String id) throws InterruptedException{
                
        String res = "{\"phone\" : \""+ inputCellNumber + "\",\"id\" : \""+ id +"\", \"res\" : \"OK\"}";           
        sendCommandsim900("AT+CMGS=\""+inputCellNumber+"\"",500,"AT",false); //skiepd error with at command              
        if(!sendCommandsim900(message+(char)26,50,"OK",true)){// fix time with readCommand skip
            System.out.println("Error sending SMS");
            res = "{\"phone\" : \""+ inputCellNumber + "\",\"id\" : \""+ id +"\", \"res\" : \"ERROR\"}";
        }
        //here will be the response
        String[] args = new String[2];
        args[0] = res;
        args[1] = "/dev/resSMS";
        try {
            Publisher.main(args);
        } catch (MqttException ex) {
            Logger.getLogger(ReadWriteCommand.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
  
    
    public void sendSMS(String json) throws JSONException, InterruptedException{
        JSONObject obj = new JSONObject(json);
        JSONArray SMSs =  obj.getJSONArray("sms");
        String message = obj.getString("message");
        
        for (int i = 0; i < SMSs.length(); i++)
        {
            String number = SMSs.getJSONObject(i).getString("phone");
            String id = SMSs.getJSONObject(i).getString("id");
            sendSMSsim900(number,message,id);
        }
    }
}
