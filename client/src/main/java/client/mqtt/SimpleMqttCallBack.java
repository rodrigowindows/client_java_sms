package client.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import client.ReadWriteCommand;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

public class SimpleMqttCallBack implements MqttCallback {
    ReadWriteCommand ReadWriteC;
    ExecutorService es;
    SimpleMqttCallBack(ReadWriteCommand ReadWriteC){
       this.ReadWriteC = ReadWriteC;
       es = Executors.newSingleThreadExecutor();
    }

  public void connectionLost(Throwable throwable) {
    System.out.println("Connection to MQTT broker lost!");
  }

  public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
    System.out.println("Message received:\t"+ new String(mqttMessage.getPayload()) );
    
    
    es.submit(() ->  {
        try {
            ReadWriteC.sendSMS(new String(mqttMessage.getPayload()));
        } catch (JSONException ex) {
            Logger.getLogger(SimpleMqttCallBack.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(SimpleMqttCallBack.class.getName()).log(Level.SEVERE, null, ex);
        }
    });
   
    //es.shutdown();
  }

  public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
  }
}
