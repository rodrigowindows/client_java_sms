package client.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Publisher {


  public static void main(String[] args) throws MqttException {

    String messageString = "No Message sent";

    if (args.length == 2 ) {
      messageString = args[0];
    }


    System.out.println("== START PUBLISHER ==");

//    String baseURL = "localhost";
    String baseURL = "18.191.149.21";

    MqttClient client = new MqttClient("tcp://"+baseURL+":1883", MqttClient.generateClientId());
    client.connect();
    MqttMessage message = new MqttMessage();
    message.setPayload(messageString.getBytes());
    client.publish(args[1], message);

    System.out.println("\tMessage '"+ messageString +"' to '"+args[1]+"'");

    client.disconnect();

    System.out.println("== END PUBLISHER ==");

  }


}
