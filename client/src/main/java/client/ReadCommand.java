/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import client.mqtt.Publisher;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.swing.JTextArea;


import java.util.Scanner;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author Mallory
 */
public class ReadCommand {
     SerialPort comPort;
     JTextArea logOutput;
     Logger logger;
     String  lineOutput = new String();
     String lineTemp = new String();
     String new_message = new String();
     String phoneNumber = new String();
     public boolean readyRead = true;
     public WriteCommand writedC;
     
     ReadCommand(JTextArea logOutput){
        this.logOutput = logOutput;
        
        initLog();
        comPort = SerialPort.getCommPorts()[0];
        comPort.openPort();
        comPort.addDataListener(new SerialPortDataListener() {
           @Override
           public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }
           @Override
           public void serialEvent(SerialPortEvent event)
           {
              if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                 return;
//              byte[] newData = new byte[comPort.bytesAvailable()];
//              int numRead = comPort.readBytes(newData, newData.length);
//            
                comPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);
                Scanner scanner = new Scanner(comPort.getInputStream());
                for (int i = 1; i < 100000; ++i)
                    if (scanner.hasNextLine())
                    {
                        lineTemp = scanner.nextLine();
                        if(!lineTemp.isEmpty()){
//                            System.out.println("Read to read: "+readyRead);
                            if( !lineTemp.contains(">") ) //will need to remove this logi to test when arrives +CMGS: if ok or error
                                readyRead = true;
                            lineOutput = lineTemp;
                            logOutput.append(lineOutput+"\n");
                            System.out.println("Full Line #" + i + ": " + lineOutput);

                            if(lineOutput.contains("+CMT:")){
                                new_message = scanner.nextLine();
                                System.out.println("message value: "+ new_message);                                                             
                                Pattern p = Pattern.compile("\"([^\"]*)\"");
                                Matcher m = p.matcher(lineOutput);
                                 if (m.find( )) {
                                    phoneNumber =  m.group(1);
                           
                                    //here will be the response
                                    String[] args = new String[2];
                                    args[0] ="{\"phone\" : \""+ phoneNumber + "\",\"new_message\" : \""+ new_message +"\"}";
                                    args[1] = "/dev/smsFromClient";
                                    try {
                                       Publisher.main(args);
                                       if(!writedC.busyWriting){//clean sms memory
                                           writedC.writeCommandAT("AT+CMGD=1,4",100);
                                           while(writedC.t1.isAlive());
                                       }
                                        
                                    } catch (MqttException ex) {
                                       Logger.getLogger(ReadWriteCommand.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    }else {
                                       System.out.println("NO MATCH");
                                    }
                             
                            }
                        }
                    }               
            }                        
        });
     
     }
     
     String getLineOutput(){
         return(lineOutput);
     }
     
     public void setreadyRead(boolean value){
         this.readyRead = value;
     }
     
     public boolean getreadyRead(){
         return (this.readyRead);
     }
     
     void cleanLineOutput(){
         lineOutput = new String();
         lineOutput="";
     }
     
     public void initLog(){
        logger = Logger.getLogger("MyLog");  
        FileHandler fh;  

        try {  

            // This block configure the logger with handler and formatter  
            fh = new FileHandler("LogFile.log",true);  
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();  
            fh.setFormatter(formatter);  
        } catch (SecurityException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
    }
}
