/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import com.fazecast.jSerialComm.SerialPort;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Mallory
 */
public class WriteCommand {
    PrintWriter output; 
    SerialPort comPort;
    Thread t1;
    public boolean busyWriting =  false;
    WriteCommand(SerialPort comPort) throws InterruptedException{
        TimeUnit.SECONDS.sleep(1);
        this.comPort = comPort;
        this.output = new PrintWriter(comPort.getOutputStream());
    }
    
     void writeCommandAT(String command, int timeMlcs){
         
        this.t1 = new Thread(new Runnable() {
            public void run()
            {
                busyWriting = true;
                output.println(command);
                output.flush();
                try {
                    Thread.sleep(timeMlcs);
                    busyWriting = false;
                } catch (InterruptedException ex) {
                    Logger.getLogger(Form.class.getName()).log(Level.SEVERE, null, ex);
                    Thread.currentThread().start();
                    Thread.currentThread().interrupt(); // restore interrupted status
                }
            }  
        });
        this.t1.start();
    }   

}